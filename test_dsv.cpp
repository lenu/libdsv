#include <gtest/gtest.h>

#include <vector>
#include <string>
#include "dsv.h"

using namespace std;

namespace
{
    // An example of a table represented in the DSV format
    const string TABLE_STRING =
        "value1:value2:value3\n"  // three simple values
        "value4\n"  // one field alone
        "\n"  // one empty field
        "::\n"  // three empty fields
        "\\n\\r\n"  // escaped newline and carriage return characters
        ":\\\n"  // one empty field and a backslash at the end of field 2
        "lastline:has\\:separator\\:twice\n"  // two fields, one has
                                               // the escaped separator
        "field with \\\\backslash\n";  // escaped backslash in a field.

    // An example of a table representated in memory
    const dsv::Table TABLE { {"value1", "value2", "value3"},
                             {"value4"},
                             {""},
                             {"", "", ""},
                             {"\n\r"},
                             {"", "\\"},
                             {"lastline", "has:separator:twice"},
                             {"field with \\backslash"} };

    // The proper serialization of TABLE.
    const string TABLE_SERIALIZATION =
        "value1:value2:value3\n"
        "value4\n"
        "\n"
        "::\n"
        "\\n\\r\n"
        ":\\\\\n"  // only difference to `TABLE_STRING`: backslash should
                   // be properly escaped with a preceeding backslash.
        "lastline:has\\:separator\\:twice\n"
        "field with \\\\backslash\n";


}

TEST(TestDSV, TestRead)
{
    istringstream input(TABLE_STRING);

    vector<vector<string>> result = dsv::read(input, ':');

    EXPECT_EQ(TABLE, result);
}

TEST(TestDSV, TestWrite)
{
    ostringstream output;
    dsv::write(TABLE, ':', output);
    EXPECT_EQ(output.str(), TABLE_SERIALIZATION);
}
