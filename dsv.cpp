#include "dsv.h"

namespace
{
    const char BACKSLASH = '\\';
    const char NEWLINE = '\n';
    const char CARRIAGE_RETURN = '\r';
}

namespace dsv
{
    Table read(std::istream &input, char delimiter)
    {
        std::string line;

        Table table;

        while (std::getline(input, line)) {
            Row current_row;
            std::string current_field;

            for (size_t i = 0; i <= line.size(); i++) {
                // End of field
                if (i == line.size() || line[i] == delimiter) {
                    current_row.emplace_back(std::move(current_field));
                } else if (line[i] == BACKSLASH) {
                    // Escaped character
                    if (i+1 < line.size()) {
                        char next = line[i+1];

                        switch (next) {
                            // Escaped newline
                            case 'n':
                                current_field += '\n';
                                break;

                            // Escaped carriage return
                            case 'r':
                                current_field += '\r';
                                break;

                            // Escaped backslash
                            case BACKSLASH:
                                current_field += BACKSLASH;
                                break;

                            default:
                                // Escaped delimiter
                                if (next == delimiter) {
                                    current_field += delimiter;
                                } else {
                                    // Ignore previous backslash
                                    current_field += BACKSLASH;
                                    current_field += next;
                                }
                        }

                        // We've consumed one extra character; account for it
                        i++;
                    } else {
                        // A backslash in the end of the line: simply append it
                        // to the current field
                        current_field += BACKSLASH;
                    }
                } else { // Regular character
                    current_field += line[i];
                }
            }

            table.emplace_back(std::move(current_row));
        }

        return table;
    }
}

namespace
{
    // Writes a field to the output stream, escaping backslashes and the
    // @delimiter by putting a backslash before them and by replacing
    // newlines for the string "\n" and carriage returns for "\r".
    void write_field(std::ostream &output, char delimiter, const std::string &field) {
        for (char c : field) {
            if (c == delimiter || c == BACKSLASH) {
                output << BACKSLASH << c;
            } else if (c == NEWLINE) {
                output << "\\n";
            } else if (c == CARRIAGE_RETURN) {
                output << "\\r";
            } else {
                output << c;
            }
        }
    }
}

namespace dsv
{
    void write(const Table &table, char delimiter, std::ostream &output)
    {
        for (const auto &row : table) {
            bool first = true;

            for (const auto &field : row) {
                if (!first) {
                    output << delimiter;
                } else {
                    first = false;
                }

                write_field(output, delimiter, field);
            }

            output << NEWLINE;
        }
    }
}
