#ifndef LIBDSV_H
#define LIBDSV_H

#include <iostream>
#include <string>
#include <vector>

namespace dsv
{
    typedef std::vector<std::string> Row;

    typedef std::vector<Row> Table;

    /** Reads a table from a delimiter-separated input stream.
      *
      * \param input The input stream to read from.
      * \param delimiter The character used to separate values in a row.
      *
      * A delimiter-separated input file is a file where each line
      * contains a list of fields separated by a certain single-character
      * delimiter. Every line contains at least one field. Lines are
      * separated by the platform's default line ending character.
      * Fields may be arbitrary strings, and special characters can be
      * escaped to be put in a field.
      *
      * If the delimiter character is preceeded by a backslash ('\'),
      * it is interpreted as a regular character, i.e. not a delimiter.
      *
      * If a backslash is preceeded by another backslash, the second backslash
      * is interpreted as a regular character, i.e. not one used to escape the
      * delimiter.
      *
      * For example, if the delimiter is the character ':', the
      * string "a:b" is interpreted as a row consisting of two values,
      * "a" and "b". The string "a\:b:c", is also interpreted as having two
      * values, "a:b" and "c", as the delimiter is escaped. However, the
      * sequence "a\\:b:c" is interpreted as a row of three values: "a\", "b"
      * and "c", as the first backslash escapes the second, which in turn
      * does not escape the delimiter.
      *
      * The newline and carriage return characters ('\n' and '\r',
      * respectively) are also interpreted in their escaped form. That means
      * the string "\n" (a backslash followed by an 'n') is interpreted as
      * a string of one character: a newline, whereas a real newline character
      * in the input stream may be interpreted as a separator between rows
      * depending on the platform.
      *
      * A backslash placed before a character that is not @delimiter,
      * 'r', 'n' or another backslash is interpreted as a regular character.
      * That is, "a\eb" is interpreted literally as a field with 4 characters,
      * "a\eb".
      *
      * If the last character in the stream is a newline character, it's
      * ignored. Otherwise, it would always create an empty row at the end.
      *
      * \return The table read from the input stream. The table may have
      * a different number of values in each row, and is represented as
      * a std::vector containing the rows. Each row is another std::vector
      * having elements of type std::string, which are the values in the row.
      */
    Table read(std::istream &input, char delimiter);

    /** Writes a table in the delimiter-separated values format.
     * \param table The table to be serialized.
     * \param delimiter The field delimiter to be used.
     * \param output The output stream to write the table to.
     *
     * Rows are separated by a newline character ('\n'). Fields are separated
     * using the @delimiter character. If this character occurs inside a
     * field, it will be preceeded by a backslash. A backslash is also inserted
     * before backslashes found inside fields.
     *
     * Newlines ('\n') and carriage returns ('\r') inside fields are replaced
     * by the two-character strings "\n" and "\r", respectively (i.e. as they
     * would be represented in a C string literal).
     *
     * A newline is output after the last row.
     */
    void write(const Table &table, char delimiter, std::ostream &output);
}

#endif
